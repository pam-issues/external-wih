/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.jbpm.process.workitem.core.AbstractLogOrThrowWorkItemHandler;
import org.jbpm.process.workitem.core.util.RequiredParameterValidator;
import org.jbpm.process.workitem.core.util.Wid;
import org.jbpm.process.workitem.core.util.WidMavenDepends;
import org.jbpm.process.workitem.core.util.WidParameter;
import org.jbpm.process.workitem.core.util.WidResult;
import org.jbpm.process.workitem.core.util.service.WidAction;
import org.jbpm.process.workitem.core.util.service.WidAuth;
import org.jbpm.process.workitem.core.util.service.WidService;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieRuntime;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;

@Wid(widfile = "external-wih.wid", name = "ExternalWorkItemHandler",
     displayName = "External WorkItemHandler",
     defaultHandler = "mvel: new com.example.ExternalWIH(\"http://localhost:8081\")",
     category = "services",
     icon = "external-wih-icon.png",
     parameters = {
                   @WidParameter(name = "SampleParam", required = false, type = "new StringDataType()"),
     },
     results = {
                @WidResult(name = "SampleResult")
     },
     mavenDepends = {
                     @WidMavenDepends(group = "com.example", artifact = "external-wih", version = "1.0-SNAPSHOT")
     },
     serviceInfo = @WidService(category = "services", description = "Delegate a workitem to an external executor",
                               keywords = "external",
                               action = @WidAction(title = "Execute a task externally"),
                               authinfo = @WidAuth(required = true, params = {"SampleParam"},
                                                   paramsdescription = {"Sample Paramiter"}
                               )
     )
)
public class ExternalWorkItemHandler extends AbstractLogOrThrowWorkItemHandler {

    private String URL;
    private KieSession kieSession;


    /**
    * 
    */
    public ExternalWorkItemHandler() {}

    /**
    * 
    */
    public ExternalWorkItemHandler(String url, KieSession kieSession) {
        this.URL = url;
        this.kieSession = kieSession;
    }

    public void executeWorkItem(WorkItem workItem,
                                WorkItemManager manager) {
        try {
            RequiredParameterValidator.validate(this.getClass(),
                                                workItem);

            Client client = ClientBuilder.newClient();
            String path = workItem.getName() + "/" + workItem.getId();
            Map<String, Object> params = workItem.getParameters();
            params.put("kieserver", System.getProperty("org.kie.server.location"));
            params.put("processInstanceId",workItem.getProcessInstanceId());
            params.put("deploymentId", kieSession.getEnvironment().get("deploymentId"));

            client.target(URL)
                  .path(path)
                  .request(MediaType.APPLICATION_JSON)
                  .post(Entity.json(params))
                  .close();

            // To fire and forget, uncomment the following
            // manager.completeWorkItem(workItem.getId(), results);
        } catch (Throwable cause) {
            handleException(cause);
        }
    }

    @Override
    public void abortWorkItem(WorkItem workItem,
                              WorkItemManager manager) {
        // stub
    }
}
